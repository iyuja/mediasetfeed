#!/bin/env python3

import configparser
from lib.feeds import feeds
from lib.mediasetplay import mediaset
from lib.s3 import client

def readConfig(
        configFile='config/mediasetplay.ini'
        ):
    config = configparser.ConfigParser()
    config.read(configFile)
    return config


print("Reading configuration")

config = readConfig()
m = mediaset()
m.apiLogin()

print("Getting Channels URLs")
f = feeds(m.generateFeed())

s3 = client()

s3.getSession(
        accessKeyId=config['s3']['accessKeyId'],
        secretAccessKey=config['s3']['secretAccessKey']
        )

print("Writing XML file to bucket")
s3.putObjectInBucket(
        body=f.xml,
        bucket=config['s3']['bucketName'],
        key='.'.join([config['s3']['fileName'], 'xml'])
        )

print("Writing M3u file to bucket")
s3.putObjectInBucket(
        body=f.m3u,
        bucket=config['s3']['bucketName'],
        key='.'.join([config['s3']['fileName'], 'm3u']),
        contentType="text/plain"
        )
