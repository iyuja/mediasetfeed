from boto3 import session as boto3

class client(object):
    def __init__(self):
        self.client = None


    def getSession(
            self,
            accessKeyId=None,
            secretAccessKey=None,
            regionName='sfo2',
            endpointUrl='digitaloceanspaces.com'
            ):

        session = boto3.Session()

        try:
            client = session.client(
                's3',
                aws_access_key_id=accessKeyId,
                aws_secret_access_key=secretAccessKey,
                endpoint_url="https://" + '.'.join([regionName, endpointUrl]),
                region_name=regionName
                )

        except Exception as err:
            print(err, file=sys.stderr)
            return {
                    'status':  'Failed',
                    'error': err
                   }

        self.client = client

        return client 


    def putObjectInBucket(
            self,
            body=None,
            bucket=None,
            key=None,
            cacheControl='max-cache=3600',
            contentType='application/xml',
            acl='public-read'
            ):

        client = self.client

        try:
            client.put_object(
                    Body=body,
                    Bucket=bucket,
                    Key=key,
                    CacheControl=cacheControl,
                    ContentType=contentType,
                    ACL=acl
                    )

            return {
                    'status': 'OK',
                    'path': '/'.join([bucket, "/", key])
                    }

        except Exception as err:
            print(err, file=sys.stderr)
            return {
                    'status': 'Failed',
                    'error': err
                    }
