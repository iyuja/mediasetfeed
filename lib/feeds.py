class feeds(object):
    def __init__(self, channels):
        self.m3u = self.generateM3U(channels)
        self.xml = self.generateXML(channels)


    def generateXML(self, channels=None):
        xml = '<?xml version = "1.0" encoding = "utf-8" standalone = "yes"?>\n'
        xml += '<Content >\n'

        for channel in channels:
            xml += '  <item\n    title = "' + channel['title'] + '"\n    description = "' +  channel['title'] + '"\n    hdposterurl = "' +  channel['iconUrl'] + '"\n    streamformat = "m3u8"\n    url = "' + channel['feedUrl'] + '" />\n'

        xml += '</Content>'
        return xml


    def generateM3U(self, channels=None):
        m3u = "#EXTM3U\n"

        for channel in channels:
            m3u += "#EXTINF:0" + "," + channel['title'] + "\n" + channel['feedUrl'] + "\n"

        return m3u
