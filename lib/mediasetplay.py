import json
import uuid
import requests
from urllib import request as urllib, error as urlliberr
from datetime import datetime, timedelta
import os
#import ssl
#import sys
import logging

class mediaset():
    def __init__(self):
        self.apiData = None
        self.reqTimeout = 10


    def resolveLiveChannelUrl(self, _callSign):
        response = None

        url = self.getLiveChannelUrl(_callSign)
        if url:
            try:
                r = urllib.urlopen(url, timeout=self.reqTimeout)
                response = r.geturl()

            except urlliberr.URLError as err:
                logging.warning("resolveLiveChannelUrl:" + err.reason)

            except urlliberr.HTTPError as err:
                logging.warning("resolveLiveChannelUrl:" + err.reason)

        return response


    def getLiveChannelUrl(self, _callSign):
        apiData = self.apiData
        respones = None

        apiUrl = "https://api-ott-prod-fe.mediaset.net/PROD/play/alive/nownext/v1.0?channelId=%callSign%".replace('%callSign%', _callSign)

        #ssl._create_default_https_context = ssl._create_unverified_context

        headers = {
            't-apigw': apiData['t-apigw'],
            't-cts': apiData['t-cts'],
            'Accept': 'application/json'
        }

        try:
            response = requests.get(apiUrl, False, headers=headers, timeout=self.reqTimeout)

        except Timeout as err:
            logging.warning("getLiveChannelUrl: timeout " + err)
        except HTTPError as err:
            logging.warning("getLiveChannelUrl: http error " + err)


        data = json.loads(response.content)

        if 'isOk' in data and data['isOk']:
            for entry in data['response']['tuningInstruction']['urn:theplatform:tv:location:any']:
                if entry['format'] == 'application/x-mpegURL':
                    return entry['publicUrls'][0]


    def getChannelList(self):
        response = None
        channelList = []

        apiUrl = 'https://feed.entertainment.tv.theplatform.eu/f/PR1GhC/mediaset-prod-all-stations?bycallsign=&range=1-1000&fields=guid,title,callSign,thumbnails,mediasetstation$pageUrl'

        try:
            response = urllib.urlopen(apiUrl, timeout=self.reqTimeout)
        except urlliberr.URLError as err:
            logging.warning("getChannelList: " + err.reason)

        except urlliberr.HTTPError as err:
            logging.warning("getChannelList: " + err.reason)

        data = json.load(response)

        if 'entries' in data:
            for entry in data['entries']:
                if 'channel_logo-100x100' in entry['thumbnails']:
                    icon = entry['thumbnails']['channel_logo-100x100']['url']
                else:
                    icon = 'DefaultVideo.png'

                channel = {
                    'iconUrl': icon,
                    'title': entry['title'],
                    'callSign': entry['callSign']
                    }

                channelList.append(channel)
        return channelList


    def apiLogin(self):
        response = None
        url = "https://api-ott-prod-fe.mediaset.net/PROD/play/idm/anonymous/login/v1.0"
        data = {
            'cid': str(uuid.uuid4()),
            "platform":"pc",
            "appName":"web/mediasetplay-web/2e96f80"
        }

        headers = {'Content-type': 'application/json'}
        response = requests.post(url, data=json.dumps(data), headers=headers)

        data = json.loads(response.content)
        if 'isOk' in data and data['isOk']:
            apiData = {
                "t-apigw": response.headers.get('t-apigw'),
                "t-cts": response.headers.get('t-cts'),
                "expire": datetime.now() + timedelta(hours=6),
                "traceCid": data['response']['traceCid'],
                "cwId": data['response']['cwId']
            }
            apiData['expire'] = apiData['expire'].strftime('%Y-%m-%d %H:%M:%S')
            self.apiData = apiData
            return apiData

 
    def generateFeed(self):
        channelFeed = []
        channelList = self.getChannelList()

        for entry in channelList:
            try:
                feedUrl = self.resolveLiveChannelUrl(entry['callSign'])
                if feedUrl:
                    logging.warning('generatedFeed: found url for: ' + entry['callSign'])
                    entry['feedUrl'] = feedUrl
                    channelFeed.append(entry)
                else:
                    logging.warning('generatedFeed: url not found for: ' + entry['callSign'])
            except Exception as err:
                logging.warning('generateFeed: ' + str(err))

        return channelFeed
